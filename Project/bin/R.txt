int attr adSize 0x7f010000
int attr adSizes 0x7f010001
int attr adUnitId 0x7f010002
int attr buyButtonAppearance 0x7f010018
int attr buyButtonHeight 0x7f010015
int attr buyButtonText 0x7f010017
int attr buyButtonWidth 0x7f010016
int attr cameraBearing 0x7f010004
int attr cameraTargetLat 0x7f010005
int attr cameraTargetLng 0x7f010006
int attr cameraTilt 0x7f010007
int attr cameraZoom 0x7f010008
int attr environment 0x7f010012
int attr fragmentMode 0x7f010014
int attr fragmentStyle 0x7f010013
int attr mapType 0x7f010003
int attr maskedWalletDetailsBackground 0x7f01001b
int attr maskedWalletDetailsButtonBackground 0x7f01001d
int attr maskedWalletDetailsButtonTextAppearance 0x7f01001c
int attr maskedWalletDetailsHeaderTextAppearance 0x7f01001a
int attr maskedWalletDetailsLogoImageType 0x7f01001f
int attr maskedWalletDetailsLogoTextColor 0x7f01001e
int attr maskedWalletDetailsTextAppearance 0x7f010019
int attr theme 0x7f010011
int attr uiCompass 0x7f010009
int attr uiRotateGestures 0x7f01000a
int attr uiScrollGestures 0x7f01000b
int attr uiTiltGestures 0x7f01000c
int attr uiZoomControls 0x7f01000d
int attr uiZoomGestures 0x7f01000e
int attr useViewLifecycle 0x7f01000f
int attr zOrderOnTop 0x7f010010
int color common_action_bar_splitter 0x7f060009
int color common_signin_btn_dark_text_default 0x7f060000
int color common_signin_btn_dark_text_disabled 0x7f060002
int color common_signin_btn_dark_text_focused 0x7f060003
int color common_signin_btn_dark_text_pressed 0x7f060001
int color common_signin_btn_default_background 0x7f060008
int color common_signin_btn_light_text_default 0x7f060004
int color common_signin_btn_light_text_disabled 0x7f060006
int color common_signin_btn_light_text_focused 0x7f060007
int color common_signin_btn_light_text_pressed 0x7f060005
int color common_signin_btn_text_dark 0x7f060017
int color common_signin_btn_text_light 0x7f060018
int color wallet_bright_foreground_disabled_holo_light 0x7f06000f
int color wallet_bright_foreground_holo_dark 0x7f06000a
int color wallet_bright_foreground_holo_light 0x7f060010
int color wallet_dim_foreground_disabled_holo_dark 0x7f06000c
int color wallet_dim_foreground_holo_dark 0x7f06000b
int color wallet_dim_foreground_inverse_disabled_holo_dark 0x7f06000e
int color wallet_dim_foreground_inverse_holo_dark 0x7f06000d
int color wallet_highlighted_text_holo_dark 0x7f060014
int color wallet_highlighted_text_holo_light 0x7f060013
int color wallet_hint_foreground_holo_dark 0x7f060012
int color wallet_hint_foreground_holo_light 0x7f060011
int color wallet_holo_blue_light 0x7f060015
int color wallet_link_text_light 0x7f060016
int color wallet_primary_text_holo_light 0x7f060019
int color wallet_secondary_text_holo_dark 0x7f06001a
int dimen activity_horizontal_margin 0x7f0a0000
int dimen activity_vertical_margin 0x7f0a0001
int drawable armor_marker 0x7f020000
int drawable attack 0x7f020001
int drawable back_btn 0x7f020002
int drawable backpack 0x7f020003
int drawable backpack2 0x7f020004
int drawable boots_empty 0x7f020005
int drawable boots_on 0x7f020006
int drawable button 0x7f020007
int drawable camp 0x7f020008
int drawable camp_marker 0x7f020009
int drawable char_btn 0x7f02000a
int drawable chest_empty 0x7f02000b
int drawable chest_on 0x7f02000c
int drawable common_signin_btn_icon_dark 0x7f02000d
int drawable common_signin_btn_icon_disabled_dark 0x7f02000e
int drawable common_signin_btn_icon_disabled_focus_dark 0x7f02000f
int drawable common_signin_btn_icon_disabled_focus_light 0x7f020010
int drawable common_signin_btn_icon_disabled_light 0x7f020011
int drawable common_signin_btn_icon_focus_dark 0x7f020012
int drawable common_signin_btn_icon_focus_light 0x7f020013
int drawable common_signin_btn_icon_light 0x7f020014
int drawable common_signin_btn_icon_normal_dark 0x7f020015
int drawable common_signin_btn_icon_normal_light 0x7f020016
int drawable common_signin_btn_icon_pressed_dark 0x7f020017
int drawable common_signin_btn_icon_pressed_light 0x7f020018
int drawable common_signin_btn_text_dark 0x7f020019
int drawable common_signin_btn_text_disabled_dark 0x7f02001a
int drawable common_signin_btn_text_disabled_focus_dark 0x7f02001b
int drawable common_signin_btn_text_disabled_focus_light 0x7f02001c
int drawable common_signin_btn_text_disabled_light 0x7f02001d
int drawable common_signin_btn_text_focus_dark 0x7f02001e
int drawable common_signin_btn_text_focus_light 0x7f02001f
int drawable common_signin_btn_text_light 0x7f020020
int drawable common_signin_btn_text_normal_dark 0x7f020021
int drawable common_signin_btn_text_normal_light 0x7f020022
int drawable common_signin_btn_text_pressed_dark 0x7f020023
int drawable common_signin_btn_text_pressed_light 0x7f020024
int drawable dziennik 0x7f020025
int drawable english 0x7f020026
int drawable fight_marker 0x7f020027
int drawable fire_marker 0x7f020028
int drawable firearm_empty 0x7f020029
int drawable firearm_on 0x7f02002a
int drawable gloves_empty 0x7f02002b
int drawable gloves_on 0x7f02002c
int drawable haed_empty 0x7f02002d
int drawable head_on 0x7f02002e
int drawable hospital_marker 0x7f02002f
int drawable ic_launcher 0x7f020030
int drawable ic_plusone_medium_off_client 0x7f020031
int drawable ic_plusone_small_off_client 0x7f020032
int drawable ic_plusone_standard_off_client 0x7f020033
int drawable ic_plusone_tall_off_client 0x7f020034
int drawable leftarrow 0x7f020035
int drawable logo 0x7f020036
int drawable male_empty 0x7f020037
int drawable male_marker 0x7f020038
int drawable male_on 0x7f020039
int drawable map_btn 0x7f02003a
int drawable mut1 0x7f02003b
int drawable mut2 0x7f02003c
int drawable mut3 0x7f02003d
int drawable mut4 0x7f02003e
int drawable mute 0x7f02003f
int drawable oldpaper 0x7f020040
int drawable plecak 0x7f020041
int drawable polish 0x7f020042
int drawable powered_by_google_dark 0x7f020043
int drawable powered_by_google_light 0x7f020044
int drawable quest_marker 0x7f020045
int drawable rightarrow 0x7f020046
int drawable run 0x7f020047
int drawable stats_background 0x7f020048
int drawable unmute 0x7f020049
int id CampName 0x7f080021
int id CampText 0x7f080020
int id EinsteinQuestion1 0x7f080027
int id StoryTextView 0x7f08003c
int id action_forgot_password 0x7f0800d0
int id action_settings 0x7f0800cd
int id ammo 0x7f080039
int id ammo44 0x7f080018
int id ammo44_val 0x7f080019
int id ammo7 0x7f08001c
int id ammo7_val 0x7f08001d
int id ammo9 0x7f08001a
int id ammo9_val 0x7f08001b
int id ammo_val 0x7f08003a
int id answer1 0x7f080028
int id answer2 0x7f080029
int id answer3 0x7f08002a
int id answer4 0x7f08002b
int id answer5 0x7f08002c
int id appName 0x7f08004b
int id attackBtn 0x7f080034
int id bigMeasureBar 0x7f080057
int id bigMeasureEmptyBtn 0x7f08005c
int id bigMeasureFullBtn 0x7f08005a
int id bigMeasureToSmallBtn 0x7f08005b
int id bigMeasureVal 0x7f080058
int id book_now 0x7f080010
int id bootsImg 0x7f0800a2
int id boots_item 0x7f0800ae
int id boots_text 0x7f0800ad
int id bountyHunterButton 0x7f080073
int id buttonAnswer 0x7f08007c
int id buttonAuthors 0x7f080050
int id buttonBackpack 0x7f08004d
int id buttonCharacter 0x7f08004e
int id buttonLine 0x7f08003d
int id buttonListClass 0x7f080079
int id buttonMap 0x7f08004c
int id buttonMute 0x7f08004f
int id buttonNext 0x7f08003e
int id buttonOk 0x7f08006c
int id buttonOkClass 0x7f080078
int id buttonPreview 0x7f08003f
int id buttonStatsOk 0x7f08009a
int id buyButton 0x7f08000a
int id buy_item 0x7f0800d1
int id buy_now 0x7f08000f
int id buy_with_google 0x7f08000e
int id campImg 0x7f08001f
int id camp_description 0x7f080022
int id centerVertLine 0x7f0800b8
int id character 0x7f080084
int id character_detail 0x7f080087
int id chestImg 0x7f0800a0
int id chest_item 0x7f0800aa
int id chest_text 0x7f0800a9
int id chr_add 0x7f08008f
int id chr_now 0x7f08008e
int id chr_rm 0x7f080096
int id classic 0x7f080011
int id courierButton 0x7f080070
int id dex_add 0x7f08008d
int id dex_now 0x7f08008c
int id dex_rm 0x7f080095
int id dexterity 0x7f080082
int id dexterity_detail 0x7f080085
int id drop_item 0x7f0800ce
int id duarbility_armor 0x7f0800ca
int id email 0x7f080047
int id empty 0x7f0800b7
int id end_add 0x7f080091
int id end_now 0x7f080090
int id end_rm 0x7f080097
int id endurance 0x7f080086
int id endurance_detail 0x7f080089
int id enemyBar 0x7f08002e
int id enemyFaceImg 0x7f080030
int id enemyHP 0x7f080033
int id enemyLine 0x7f080038
int id enemyName 0x7f08002f
int id english 0x7f080042
int id eqChangeBtn 0x7f080035
int id equipment_text 0x7f0800a4
int id exitMeasureBtn 0x7f080061
int id firearmImg 0x7f08009f
int id gladiatorButton 0x7f08006f
int id glovesImg 0x7f0800a1
int id gloves_item 0x7f0800ac
int id gloves_text 0x7f0800ab
int id grayscale 0x7f080012
int id guardButton 0x7f080072
int id headImg 0x7f08009e
int id helm_item 0x7f0800a8
int id helm_text 0x7f0800a7
int id holo_dark 0x7f080005
int id holo_light 0x7f080006
int id hunterButton 0x7f080071
int id hybrid 0x7f080004
int id itemDetails 0x7f0800cc
int id itemName 0x7f0800cb
int id languageLine 0x7f080040
int id listCombatLog 0x7f08003b
int id listEquipment 0x7f08001e
int id location 0x7f080051
int id login 0x7f080049
int id login_form 0x7f080046
int id login_status 0x7f080044
int id login_status_message 0x7f080045
int id logoView 0x7f080043
int id logoView2 0x7f08006d
int id maleImg 0x7f0800a3
int id map 0x7f080053
int id match_parent 0x7f08000c
int id medLine 0x7f080059
int id medicinesLine 0x7f080056
int id merchant 0x7f080024
int id merchantEQ 0x7f080068
int id merchantLine 0x7f080062
int id merchant_text 0x7f080066
int id money_back_text 0x7f080014
int id money_back_val 0x7f080015
int id money_item 0x7f0800b0
int id money_text 0x7f0800af
int id monochrome 0x7f080013
int id nickName 0x7f08006b
int id none 0x7f080000
int id normal 0x7f080001
int id ok 0x7f08007d
int id pager 0x7f080069
int id password 0x7f080048
int id playerBar 0x7f08002d
int id playerEQ 0x7f080067
int id playerFightName 0x7f080031
int id playerHP 0x7f080032
int id playerLine 0x7f080037
int id playerName 0x7f08009c
int id playerProfession 0x7f08009d
int id player_money 0x7f080064
int id player_money_val 0x7f080065
int id player_text 0x7f080063
int id polish 0x7f080041
int id production 0x7f080007
int id professionChoice 0x7f08006e
int id professionInfo 0x7f080074
int id profession_bonus 0x7f080077
int id profession_detail 0x7f080076
int id profession_name 0x7f080075
int id put_item 0x7f0800cf
int id quest 0x7f080023
int id quest_answer 0x7f08007b
int id quest_text 0x7f08007a
int id questionText 0x7f080026
int id repSukiennice_text 0x7f0800b1
int id repSukiennice_val 0x7f0800b2
int id repUP_text 0x7f0800b5
int id repUP_val 0x7f0800b6
int id repWawel_text 0x7f0800b3
int id repWawel_val 0x7f0800b4
int id run 0x7f080036
int id sandbox 0x7f080008
int id satellite 0x7f080002
int id selectionDetails 0x7f08000b
int id sell_item 0x7f0800d2
int id setNickName 0x7f08006a
int id set_stats 0x7f08007e
int id sign_in_button 0x7f08004a
int id smallMeasureBar 0x7f080054
int id smallMeasureEmptyBtn 0x7f08005f
int id smallMeasureFullBtn 0x7f08005d
int id smallMeasureToBigBtn 0x7f08005e
int id smallMeasureVal 0x7f080055
int id spd_add 0x7f080093
int id spd_now 0x7f080092
int id spd_rm 0x7f080098
int id speed 0x7f080088
int id speed_detail 0x7f080099
int id stats_armor 0x7f0800c8
int id stats_armor_label 0x7f0800c7
int id stats_attack 0x7f0800c6
int id stats_attack_label 0x7f0800c5
int id stats_chr 0x7f0800c0
int id stats_chr_label 0x7f0800bf
int id stats_dex 0x7f0800be
int id stats_dex_label 0x7f0800bd
int id stats_durability_label 0x7f0800c9
int id stats_end 0x7f0800c2
int id stats_end_label 0x7f0800c1
int id stats_health 0x7f0800ba
int id stats_health_label 0x7f0800b9
int id stats_left 0x7f080080
int id stats_points 0x7f08007f
int id stats_spd 0x7f0800c4
int id stats_spd_label 0x7f0800c3
int id stats_str 0x7f0800bc
int id stats_str_label 0x7f0800bb
int id str_add 0x7f08008b
int id str_now 0x7f08008a
int id str_rm 0x7f080094
int id strength 0x7f080081
int id strength_detail 0x7f080083
int id strict_sandbox 0x7f080009
int id terrain 0x7f080003
int id textView 0x7f08009b
int id timer 0x7f080052
int id tmp 0x7f080025
int id upReward2 0x7f080060
int id weapon_item 0x7f0800a6
int id weapon_text 0x7f0800a5
int id weight_text 0x7f080016
int id weight_val 0x7f080017
int id wrap_content 0x7f08000d
int integer google_play_services_version 0x7f090000
int layout activity_authors 0x7f030000
int layout activity_backpack 0x7f030001
int layout activity_camp 0x7f030002
int layout activity_einstein 0x7f030003
int layout activity_fight 0x7f030004
int layout activity_intro 0x7f030005
int layout activity_language 0x7f030006
int layout activity_login 0x7f030007
int layout activity_main 0x7f030008
int layout activity_map 0x7f030009
int layout activity_medicines 0x7f03000a
int layout activity_merchant 0x7f03000b
int layout activity_player 0x7f03000c
int layout activity_player_name 0x7f03000d
int layout activity_profession_choice 0x7f03000e
int layout activity_profession_details 0x7f03000f
int layout activity_quest 0x7f030010
int layout activity_set_stats 0x7f030011
int layout combat_log 0x7f030012
int layout fragment_player_character 0x7f030013
int layout fragment_player_statistics 0x7f030014
int layout list_backpack 0x7f030015
int layout list_merchant 0x7f030016
int menu authors 0x7f0b0000
int menu backpack 0x7f0b0001
int menu backpack_item 0x7f0b0002
int menu camp 0x7f0b0003
int menu character 0x7f0b0004
int menu einstein 0x7f0b0005
int menu fight 0x7f0b0006
int menu language 0x7f0b0007
int menu login 0x7f0b0008
int menu main 0x7f0b0009
int menu map 0x7f0b000a
int menu medicines 0x7f0b000b
int menu merchant 0x7f0b000c
int menu merchant_item 0x7f0b000d
int menu player_item 0x7f0b000e
int menu player_name 0x7f0b000f
int menu profession_choice 0x7f0b0010
int menu profession_details 0x7f0b0011
int menu quest 0x7f0b0012
int menu set_stats 0x7f0b0013
int raw areyouhurt 0x7f040000
int raw backpack 0x7f040001
int raw fix 0x7f040002
int raw giant3 0x7f040003
int raw hello 0x7f040004
int raw merc1 0x7f040005
int raw merc2 0x7f040006
int raw merc3 0x7f040007
int raw merc4 0x7f040008
int raw reload 0x7f040009
int raw run 0x7f04000a
int raw shot 0x7f04000b
int raw soundtrack 0x7f04000c
int raw sword_hit 0x7f04000d
int raw sword_miss 0x7f04000e
int string EinsteinQuestion1 0x7f07006f
int string EinsteinQuestion1Answer1 0x7f070070
int string EinsteinQuestion1Answer2 0x7f070071
int string EinsteinQuestion1Answer3 0x7f070072
int string EinsteinQuestion1Answer4 0x7f070073
int string EinsteinQuestion1Answer5 0x7f070074
int string EinsteinQuestion2 0x7f070075
int string EinsteinQuestion2Answer1 0x7f070076
int string EinsteinQuestion2Answer2 0x7f070077
int string EinsteinQuestion2Answer3 0x7f070078
int string EinsteinQuestion2Answer4 0x7f070079
int string EinsteinQuestion2Answer5 0x7f07007a
int string EinsteinQuestion3 0x7f07007b
int string EinsteinQuestion3Answer1 0x7f07007c
int string EinsteinQuestion3Answer2 0x7f07007d
int string EinsteinQuestion3Answer3 0x7f07007e
int string EinsteinQuestion3Answer4 0x7f07007f
int string EinsteinQuestion3Answer5 0x7f070080
int string EinsteinQuestion4 0x7f070081
int string EinsteinQuestion4Answer1 0x7f070082
int string EinsteinQuestion4Answer2 0x7f070083
int string EinsteinQuestion4Answer3 0x7f070084
int string EinsteinQuestion4Answer4 0x7f070085
int string EinsteinQuestion4Answer5 0x7f070086
int string action_forgot_password 0x7f0700d2
int string action_settings 0x7f07001b
int string action_sign_in_register 0x7f0700d0
int string action_sign_in_short 0x7f0700d1
int string ammo 0x7f0700b8
int string ammo44 0x7f0700b5
int string ammo7 0x7f0700b7
int string ammo9 0x7f0700b6
int string answer 0x7f0700a5
int string app_name 0x7f07001a
int string armor 0x7f07004d
int string attack 0x7f0700af
int string authors 0x7f0700ca
int string background 0x7f0700c7
int string bigMeasureEmptyBtn 0x7f0700bc
int string bigMeasureFullBtn 0x7f0700bb
int string bigMeasureToSmallBtn 0x7f0700bd
int string boots 0x7f07005a
int string bountyhunter_bonus 0x7f070042
int string bountyhunter_details 0x7f070041
int string button_Next 0x7f070024
int string button_Preview 0x7f070025
int string button_answer 0x7f0700a6
int string button_backpack 0x7f070022
int string button_bountyhunter 0x7f070032
int string button_courier 0x7f07002f
int string button_gladiator 0x7f07002e
int string button_guard 0x7f070031
int string button_hunter 0x7f070030
int string button_map 0x7f070023
int string button_ok 0x7f070038
int string button_player 0x7f070021
int string buy_item 0x7f0700b9
int string camp_name 0x7f07005c
int string camp_sukiennice 0x7f070062
int string camp_sukiennice_description 0x7f070063
int string camp_up 0x7f070066
int string camp_up_description 0x7f070067
int string camp_wawel 0x7f070064
int string camp_wawel_description 0x7f070065
int string character 0x7f070049
int string character_detail 0x7f070052
int string chest 0x7f070058
int string common_google_play_services_enable_button 0x7f07000b
int string common_google_play_services_enable_text 0x7f07000a
int string common_google_play_services_enable_title 0x7f070009
int string common_google_play_services_error_notification_requested_by_msg 0x7f070004
int string common_google_play_services_install_button 0x7f070008
int string common_google_play_services_install_text_phone 0x7f070006
int string common_google_play_services_install_text_tablet 0x7f070007
int string common_google_play_services_install_title 0x7f070005
int string common_google_play_services_invalid_account_text 0x7f070011
int string common_google_play_services_invalid_account_title 0x7f070010
int string common_google_play_services_needs_enabling_title 0x7f070003
int string common_google_play_services_network_error_text 0x7f07000f
int string common_google_play_services_network_error_title 0x7f07000e
int string common_google_play_services_notification_needs_installation_title 0x7f070001
int string common_google_play_services_notification_needs_update_title 0x7f070002
int string common_google_play_services_notification_ticker 0x7f070000
int string common_google_play_services_unknown_issue 0x7f070012
int string common_google_play_services_unsupported_date_text 0x7f070015
int string common_google_play_services_unsupported_text 0x7f070014
int string common_google_play_services_unsupported_title 0x7f070013
int string common_google_play_services_update_button 0x7f070016
int string common_google_play_services_update_text 0x7f07000d
int string common_google_play_services_update_title 0x7f07000c
int string common_signin_button_text 0x7f070017
int string common_signin_button_text_long 0x7f070018
int string courier_bonus 0x7f07003e
int string courier_details 0x7f07003d
int string credits 0x7f0700cc
int string damage 0x7f07004c
int string dexterity 0x7f070048
int string dexterity_detail 0x7f070051
int string drop_item 0x7f070027
int string durability 0x7f0700ac
int string endurance 0x7f07004a
int string endurance_detail 0x7f070053
int string enemyFace 0x7f0700c8
int string english 0x7f0700c5
int string eqChange 0x7f0700b1
int string equipment 0x7f070055
int string error_field_required 0x7f0700d7
int string error_incorrect_password 0x7f0700d6
int string error_invalid_email 0x7f0700d4
int string error_invalid_password 0x7f0700d5
int string exit 0x7f0700a8
int string gladiator_bonus 0x7f07003a
int string gladiator_details 0x7f070039
int string gloves 0x7f070059
int string guard_bonus 0x7f070040
int string guard_details 0x7f07003f
int string health 0x7f070046
int string hello_world 0x7f070060
int string helm 0x7f070057
int string hunter_bonus 0x7f07003c
int string hunter_details 0x7f07003b
int string introChar_text 0x7f07002b
int string intro_text 0x7f07002a
int string login_progress_signing_in 0x7f0700d3
int string logo 0x7f0700c2
int string merchant 0x7f07005e
int string merchant_eq 0x7f0700b3
int string money_text 0x7f0700a7
int string nick 0x7f070037
int string player_eq 0x7f0700b2
int string polish 0x7f0700c4
int string professionChoser 0x7f070033
int string professionInfo 0x7f070034
int string prompt_email 0x7f0700ce
int string prompt_password 0x7f0700cf
int string put_item 0x7f070028
int string quest 0x7f07005d
int string quest_sukiennice_1 0x7f070091
int string quest_sukiennice_2 0x7f070092
int string quest_sukiennice_3 0x7f070093
int string quest_sukiennice_4 0x7f070094
int string quest_up_1 0x7f070068
int string quest_up_2 0x7f070069
int string quest_up_3 0x7f07006a
int string quest_up_4 0x7f07006b
int string quest_up_5 0x7f07006c
int string quest_up_6 0x7f07006d
int string quest_wawel_1 0x7f070099
int string quest_wawel_2 0x7f07009a
int string quest_wawel_3 0x7f07009b
int string quest_wawel_4 0x7f07009c
int string quest_wawel_5 0x7f07009d
int string quest_wawel_6 0x7f07009e
int string question_up_6 0x7f07006e
int string repSukiennice_text 0x7f0700a9
int string repUP_text 0x7f0700ab
int string repWawel_text 0x7f0700aa
int string reward_su_1 0x7f070095
int string reward_su_2 0x7f070096
int string reward_su_3 0x7f070097
int string reward_su_4 0x7f070098
int string reward_up_1 0x7f07008b
int string reward_up_2 0x7f07008c
int string reward_up_3 0x7f07008d
int string reward_up_4 0x7f07008e
int string reward_up_5 0x7f07008f
int string reward_up_6 0x7f070090
int string reward_waw_1 0x7f07009f
int string reward_waw_2 0x7f0700a0
int string reward_waw_3 0x7f0700a1
int string reward_waw_4 0x7f0700a2
int string reward_waw_5 0x7f0700a3
int string reward_waw_6 0x7f0700a4
int string run 0x7f0700b0
int string selectLang 0x7f0700c6
int string sell_item 0x7f0700b4
int string service_name_ug 0x7f070026
int string set_name 0x7f070036
int string set_stats 0x7f070044
int string smallMeasureEmptyBtn 0x7f0700bf
int string smallMeasureFullBtn 0x7f0700be
int string smallMeasureToBigBtn 0x7f0700c0
int string sound 0x7f0700c9
int string speed 0x7f07004b
int string speed_detail 0x7f070054
int string stats_add 0x7f07004e
int string stats_points 0x7f070045
int string stats_rm 0x7f07004f
int string strength 0x7f070047
int string strength_detail 0x7f070050
int string title_activity_authors 0x7f0700cb
int string title_activity_camp 0x7f07005b
int string title_activity_einstein 0x7f0700c1
int string title_activity_equipemnt 0x7f07001f
int string title_activity_fight 0x7f0700ae
int string title_activity_intro 0x7f070029
int string title_activity_language 0x7f0700c3
int string title_activity_login 0x7f0700cd
int string title_activity_map 0x7f070020
int string title_activity_medicines 0x7f0700ba
int string title_activity_merchant 0x7f070061
int string title_activity_player 0x7f07001e
int string title_activity_player_name 0x7f070035
int string title_activity_profession_choice 0x7f07002c
int string title_activity_profession_details 0x7f07002d
int string title_activity_quest 0x7f07005f
int string title_activity_set_stats 0x7f070043
int string title_player_character 0x7f07001c
int string title_player_statistics 0x7f07001d
int string up1_answer 0x7f070087
int string up2_answer 0x7f070088
int string up3_answer 0x7f070089
int string up4_answer 0x7f07008a
int string wallet_buy_button_place_holder 0x7f070019
int string weapon 0x7f070056
int string weight_text 0x7f0700ad
int style AppBaseTheme 0x7f050005
int style AppTheme 0x7f050006
int style LoginFormContainer 0x7f050007
int style Theme_IAPTheme 0x7f050000
int style WalletFragmentDefaultButtonTextAppearance 0x7f050003
int style WalletFragmentDefaultDetailsHeaderTextAppearance 0x7f050002
int style WalletFragmentDefaultDetailsTextAppearance 0x7f050001
int style WalletFragmentDefaultStyle 0x7f050004
int[] styleable AdsAttrs { 0x7f010000, 0x7f010001, 0x7f010002 }
int styleable AdsAttrs_adSize 0
int styleable AdsAttrs_adSizes 1
int styleable AdsAttrs_adUnitId 2
int[] styleable MapAttrs { 0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007, 0x7f010008, 0x7f010009, 0x7f01000a, 0x7f01000b, 0x7f01000c, 0x7f01000d, 0x7f01000e, 0x7f01000f, 0x7f010010 }
int styleable MapAttrs_cameraBearing 1
int styleable MapAttrs_cameraTargetLat 2
int styleable MapAttrs_cameraTargetLng 3
int styleable MapAttrs_cameraTilt 4
int styleable MapAttrs_cameraZoom 5
int styleable MapAttrs_mapType 0
int styleable MapAttrs_uiCompass 6
int styleable MapAttrs_uiRotateGestures 7
int styleable MapAttrs_uiScrollGestures 8
int styleable MapAttrs_uiTiltGestures 9
int styleable MapAttrs_uiZoomControls 10
int styleable MapAttrs_uiZoomGestures 11
int styleable MapAttrs_useViewLifecycle 12
int styleable MapAttrs_zOrderOnTop 13
int[] styleable WalletFragmentOptions { 0x7f010011, 0x7f010012, 0x7f010013, 0x7f010014 }
int styleable WalletFragmentOptions_environment 1
int styleable WalletFragmentOptions_fragmentMode 3
int styleable WalletFragmentOptions_fragmentStyle 2
int styleable WalletFragmentOptions_theme 0
int[] styleable WalletFragmentStyle { 0x7f010015, 0x7f010016, 0x7f010017, 0x7f010018, 0x7f010019, 0x7f01001a, 0x7f01001b, 0x7f01001c, 0x7f01001d, 0x7f01001e, 0x7f01001f }
int styleable WalletFragmentStyle_buyButtonAppearance 3
int styleable WalletFragmentStyle_buyButtonHeight 0
int styleable WalletFragmentStyle_buyButtonText 2
int styleable WalletFragmentStyle_buyButtonWidth 1
int styleable WalletFragmentStyle_maskedWalletDetailsBackground 6
int styleable WalletFragmentStyle_maskedWalletDetailsButtonBackground 8
int styleable WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance 7
int styleable WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance 5
int styleable WalletFragmentStyle_maskedWalletDetailsLogoImageType 10
int styleable WalletFragmentStyle_maskedWalletDetailsLogoTextColor 9
int styleable WalletFragmentStyle_maskedWalletDetailsTextAppearance 4
